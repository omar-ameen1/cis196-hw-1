# Name: 'Omar Ameen'

# Homework 1
#
# Numerous methods are defined below but are incomplete. For this homework
# assignment, you will have to implement the methods so that they pass the
# tests. Please don't change the method names as doing so will make the
# autograder give you a zero for the method.
#
# The Ruby docs will be very helpful for this assignment.
# Check out the docs here: http://ruby-doc.org/core-2.5.1/

# 1. In the find_evens method, you will return an array of all elements of
# the array that are even numbers in the same order. If an element is not an
# integer, it should not be included in the final array. For example,
# find_evens([2, 3, 4, 'foo']) should return `[2, 4]`.
def find_evens(elements)
  even = []
  elements.each do |item|
    if item.is_a? Integer and item.even?
      even << item
    end
  end
  even
end

# 2. In the product method, you will find the product of all elements in
# the array. If an element is not a number, just ignore it. The method should
# return 0 if it doesn't contain any numbers. For example, product([1, 2, 3])
# should return 6.
def product(elements)
  called = false
  currnum = 1
  elements.each do |item|
    called = true, currnum *= item if item.is_a? Integer or item.is_a? Float
  end
  called ? currnum : 0
end

# 3. In the uniq method, you will return an array of distinct elements from the
# original array passed into the method in the same order. If a duplicate
# element is found, it should be removed (e.g. uniq([1, 2, 1]) should return
# [1, 2]). The uniq and uniq! methods built into the Array class will be
# disabled.
def uniq(array)
  uniqarr = []
  array.each do |item|
    uniqarr << item unless uniqarr.include? item
  end
  uniqarr
end

# 4. In the invert method, you will invert a hash so that the values become the
# keys, and the keys become the corresponding values. For example,
# invert({ 1 => 'foo', 2 => 'bar' }) will return {'foo' => 1, 'bar' => 2}. If
# there are duplicate values, the most recent key-value pair should be used, so
# invert({ 1 => 'foo', 2 => 'bar', 3 => 'foo' }) will return
# {'foo' => 3, 'bar' => 2}. The built-in invert method in the Hash class will be
# disabled.
def invert(hash)
  inverted = {}
  hash.each { |key, value| inverted[value] = key }
  inverted
end

# 5. In the fetch method, you will implement something similar to Hash's
# built-in fetch method (And of course, do not use the built-in method).
# This method should return the value corresponding to the key (the second
# argument) in the hash (the first argument). If the key is not found in the
# hash, then the string 'missing' should be returned unless an optional third
# argument is passed in. If a third argument is passed in, and the key is not
# found in the hash, then that third argument should be returned.
# Hint: While you may not change the method name, you may
# change the arguments as long as the method can accept the same number of
# arguments.
def fetch(hash, key, default = 'missing')
  hash.each do |input, value|
    return value if key == input
  end
  default
end
