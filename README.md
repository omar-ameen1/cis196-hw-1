# Homework 1: Finger Exercises

**Due: 5:15pm, January.31st, 2022 (Monday)**

This assignment is designed to get you started with writing code in Ruby and submitting assignments with Git. You will be given five methods to implement. To test them for yourself, open up your terminal and run the following commands in the same order:
- `gem install bundler` to install the gem called _bundler_. Remember a gem is just a library and _bundler_ is the gem that we use to easily install all the gems in our Gemfile as we'll do in the next step.
- `bundle install` to install all of the gems in the Gemfile
- run `ruby bin/console.rb`. A Pry REPL (very similar to `irb` that we saw in class) will start, and your five methods will already be loaded! You can call any method from your HW file in this REPL. For example, for the method  _find_evens_ which takes in an array, we'd call this method in the REPL like so: `find_evens([2, 3, 4, 'foo'])`. You can examine the object returned from the method call to verify your implementation. To exit the REPL, use the `exit` command.

To check your style, run `rubocop`. If you have any style offenses, you'll get a list of them that you can then fix.
To run the test suite, run `rspec`. This will show you which tests you're passing and which you're not.

### 1. Find all even numbers
In the find\_evens method, you will return an array of all elements of the array that are even numbers in the same order. If an element is not an integer, it should not be included in the final array. For example, `find_evens([2, 3, 4, 'foo'])` should return [2, 4].

### 2. Find the product
In the product method, you will find the product of all numbers in the array. If an element is not a number, just ignore it. The method should return 0 if it doesn't contain any numbers. For example, `product([1, 2, 3])` should return 6.

### 3. Find all unique elements
In the uniq method, you will return an array of distinct elements from the original array passed into the method in the same order. If a duplicate element is found, it should be removed (e.g. `uniq([1, 2, 1])` should return [1, 2]). The uniq and uniq! methods built into the Array class will be disabled.

### 4. Invert the hash
In the invert method, you will invert a hash so that the values become the keys, and the keys become the corresponding values. For example, `invert({ 1 => 'foo', 2 => 'bar' })` will return {'foo' => 1, 'bar' => 2}. If there are duplicate values, the most recent key-value pair should be used, so `invert({ 1 => 'foo', 2 => 'bar', 3 => 'foo' })` will return {'foo' => 3, 'bar' => 2}. The built-in invert method in the Hash class will be disabled.

### 5. Fetch from the hash
In the fetch method, you will implement something similar to Hash's built-in fetch method (And of course, do not use the built-in method). This method should return the value corresponding to the key (the second argument) in the hash (the first argument). If the key is not found in the hash, then the string 'missing' should be returned unless an optional third argument is passed in. If a third argument is passed in, and the key is not found in the hash, then that third argument should be returned. Hint: While you may not change the method name, you may change the arguments as long as the method can accept the same number of arguments.

## Submitting
Make sure you've added your name to the top of `exercises.rb`. Be sure to run `rubocop` and `rspec` to catch any errors that need to be fixed before submitting.

To submit:
1. Commit your changes and push your changes to Gitlab.
2. Make a zip for this folder, and upload it on Canvas.
3. Submit only the `exercises.rb` file to Gradescope.
